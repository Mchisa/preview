$(function() {
    var $win = $(window),
        $winH = $(document).height(),
        current = 0,
        scroll;
    var catAnimation = function () {
      scroll = $win.scrollTop();
      current = scroll / $winH * 100;
      if (current > 99.9) {
        current = 100;
      }
      if (current > 10) { // 10%スクロールした時点
        $('#para').css('background-image','url(./images/parallax/3.png)');
      } else if (current > 5) { 
        $('#para').css('background-image','url(./images/parallax/2.png)');
        $('#para').animate({"right": 8 + "px"}, 100);
      } else if (current > 0) {
      	$('#para').css('background-image','url(./images/parallax/1.png)');
        $('#para').animate({"right": 0 }, 100);
      } 
      	$('#para').stop(true, true);
		$('#para').animate({"top": current + 5 + "%"}, 100); // +5は初期位置に応じて設定して下さい（今は適当）
    };
	$win.scroll(catAnimation);

  $(document).ready(function(){ //画像プリロード
    data = new Array("./image/parallax/1.png","./image/parallax/2.png","./image/parallax/3.png","./image/parallax/4.png","./image/parallax/5.png","./image/parallax/6.png","./image/parallax/7.png","./image/parallax/8.png","./image/parallax/9.png","./image/parallax/10.png","./image/parallax/11.png","./image/parallax/12.png","./image/parallax/13.png","./image/parallax/14.png","./image/parallax/15.png","./image/parallax/16.png");
    prImg= new Array();
    for (i=0; i<data.length; i++)
    {
      prImg[i] = new Image();
      prImg[i].src = data[i];
    }
  });  
});

// $(function() {
// 	var scrollTop = $(this).scrollTop();
// 	var s = 34;

// 	// PCやAndroid、iOS 8以上では画面スクロール操作に応じてスクロール量が連続的に出力される
// 	console.log(scrollTop);
// 	if(0<scrollTopscrollTop){
// 		$('#para').animate({"top":"4%","right":"0"});
// 		$('#para').css('background-image','url(./images/parallax/1.png)');
// 	}if(s<scrollTopscrollTop){
// 		$('#para').animate({"top":"10%","right":"8px"});
// 		$('#para').css('background-image','url(./images/parallax/2.png)');
// 	}if(s*2<scrollTopscrollTop){
// 		$('#para').animate({"top":"20%","right":"2px"});
// 		$('#para').css('background-image','url(./images/parallax/3.png)');
// 	}if(s*3<scrollTopscrollTop){
// 		$('#para').animate({"top":"25%","right":"10px"});
// 		$('#para').css('background-image','url(./images/parallax/4.png)');
// 	}if(s*4<scrollTopscrollTop){
// 		$('#para').animate({"top":"30%","right":"5px"});
// 		$('#para').css('background-image','url(./images/parallax/5.png)');
// 	}if(s*5<scrollTopscrollTop){
// 		$('#para').animate({"top":"35%","right":"0"});
// 		$('#para').css('background-image','url(./images/parallax/6.png)');
// 	}if(s*6<scrollTopscrollTop){
// 		$('#para').animate({"top":"40%","right":"6px"});
// 		$('#para').css('background-image','url(./images/parallax/7.png)');
// 	}if(s*7<scrollTopscrollTop){
// 		$('#para').animate({"top":"45%","right":"12px"});
// 		$('#para').css('background-image','url(./images/parallax/8.png)');
// 	}if(s*8<scrollTopscrollTop){
// 		$('#para').animate({"top":"50%","right":"4px"});
// 		$('#para').css('background-image','url(./images/parallax/9.png)');
// 	}if(s*9<scrollTopscrollTop){
// 		$('#para').animate({"top":"55%","right":"0"});
// 		$('#para').css('background-image','url(./images/parallax/10.png)');
// 	}if(s*10<scrollTopscrollTop){
// 		$('#para').animate({"top":"60%","right":"6px"});
// 		$('#para').css('background-image','url(./images/parallax/11.png)');
// 	}if(s*11<scrollTopscrollTop){
// 		$('#para').animate({"top":"65%","right":"10px"});
// 		$('#para').css('background-image','url(./images/parallax/12.png)');
// 	}if(s*12<scrollTopscrollTop){
// 		$('#para').animate({"top":"71%","right":"4px"});
// 		$('#para').css('background-image','url(./images/parallax/13.png)');
// 	}if(s*13<scrollTopscrollTop){
// 		$('#para').animate({"top":"76%","right":"0"});
// 		$('#para').css('background-image','url(./images/parallax/14.png)');
// 	}if(s*14<scrollTopscrollTop){
// 		$('#para').animate({"top":"81%","right":"5px"});
// 		$('#para').css('background-image','url(./images/parallax/15.png)');
// 	}if(s*15<scrollTopscrollTop){
// 		$('#para').animate({"top":"87%","right":"0"});
// 		$('#para').css('background-image','url(./images/parallax/16.png)');
// 	}

// 	// if(0<=scrollTop && scrollTop<s){
// 	// 	console.log("1");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/1.png)"});
// 	// }if(s<=scrollTop && scrollTop<s*2){
// 	// 	console.log("2に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/2.png)"});
// 	// }if(s*2<=scrollTop && scrollTop<s*3){
// 	// 	console.log("3に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/3.png)"});
// 	// if(s*3<=scrollTop && scrollTop<s*4){
// 	// 	console.log("4に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/4.png)"});
// 	// if(s*4<=scrollTop && scrollTop<s*5){
// 	// 	console.log("5に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/5.png)"});
// 	// }if(s*5<=scrollTop && scrollTop<s*6){
// 	// 	console.log("6に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/6.png)"});
// 	// }if(s*6<=scrollTop && scrollTop<s*7){
// 	// 	console.log("7に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/7.png)"});
// 	// }if(s*7<=scrollTop && scrollTop<s*8){
// 	// 	console.log("8に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/8.png)"});
// 	// }if(s*8<=scrollTop && scrollTop<s*9){
// 	// 	console.log("9に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/9.png)"});
// 	// }if(s*9<=scrollTop && scrollTop<s*10){
// 	// 	console.log("10に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/10.png)"});
// 	// }if(s*10<=scrollTop && scrollTop<s*11){
// 	// 	console.log("11に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/11.png)"});
// 	// }if(s*11<=scrollTop && scrollTop<s*12){
// 	// 	console.log("12に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/12.png)"});
// 	// }if(s*12<=scrollTop && scrollTop<s*13){
// 	// 	console.log("13に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/13.png)"});
// 	// }if(s*13<=scrollTop && scrollTop<s*14){
// 	// 	console.log("14に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/14.png)"});
// 	// }if(s*14<=scrollTop && scrollTop<s*15){
// 	// 	console.log("15に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/15.png)"});
// 	// }if(s*15<=scrollTop && scrollTop<s*16){
// 	// 	console.log("16に変える");
// 	// 	$('#para').css({"background-image":"url(./images/parallax/16.png)"});
// 	// }
// });